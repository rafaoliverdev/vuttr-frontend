# VUTTR
A frontend project made with [Nuxt.js](https://github.com/nuxt/nuxt.js/).
Nuxt.js is a Vue.js based framework great for SEO.

[DEMO](https://vuttr-frontend-gipnrypaqd.now.sh/)

Style:
SCSS with BEM methodology and [Tailwind CSS](https://tailwindcss.com/).


## Build Setup
``` bash

# install dependencies

$ yarn install
# or
$ npm install
  

# serve with hot reload

$ yarn run dev
# or
$ npm run dev
  

# build for production and launch server

$ yarn run build
$ yarn start
# or
$ npm run build
$ npm start
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).