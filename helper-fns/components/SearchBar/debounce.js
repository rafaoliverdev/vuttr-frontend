var delayTheSearch = null

export default function(callback, delay) {
  clearTimeout(delayTheSearch)

  delayTheSearch = setTimeout(callback, delay)
}
