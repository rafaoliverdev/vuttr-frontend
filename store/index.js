import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      tools: [],
      loading: true,
      searchArray: [],
      tagsOnly: false
    }),
    mutations: {
      setLoading(state, payload) {
        state.loading = payload
      },
      setTools(state, payload) {
        state.tools = payload
      },
      setSearchArray(state, payload) {
        state.searchArray = payload
      },
      setTagsOnly(state, payload) {
        state.tagsOnly = payload
      }
    },
    actions: {
      async nuxtServerInit({ commit }) {
        commit('setLoading', true)

        try {
          const result = await this.$axios.get('/tools')
          commit('setTools', result.data)
          commit('setLoading', false)
        } catch (error) {
          console.log(error)
          commit('setLoading', false)
        }
      },
      async deleteTool({ commit, dispatch }, id) {
        commit('setLoading', true)

        try {
          await this.$axios.delete('/tools/' + id)
          const result = await dispatch('getAllTools')
          commit('setTools', result.data)
          commit('setLoading', false)
        } catch (error) {
          console.log(error)
          commit('setLoading', false)
        }
      },
      async getAllTools({ commit }, commitOrNot) {
        commit('setLoading', true)

        try {
          const result = await this.$axios.get('/tools')

          if (commitOrNot) {
            commit('setTools', result.data)
            commit('setLoading', false)
          } else {
            return result
          }
        } catch (error) {
          console.log(error)
          commit('setLoading', false)
        }
      },
      async addNewTool({ commit, dispatch }, payload) {
        commit('setLoading', true)

        try {
          await this.$axios.post('/tools', payload)
          const result = await dispatch('getAllTools')
          commit('setTools', result.data)
          commit('setLoading', false)
        } catch (error) {
          console.log(error)
          commit('setLoading', false)
        }
      },
      async searchTools({commit}, payload) {
        commit('setLoading', true)

        let searchOption = '/tools/?q='

        if (payload.tags) {
          searchOption = '/tools/?tags_like='
        }

        let tempArrIndex = []
        let finalResult = []

        try {
          // Loop for each searched word
          for (let item of payload.searchArray) {
            let result = await this.$axios.get(searchOption + item)
            // Proceeds only if the Backend response is not empty
            if (result.data.length !== 0) {
              // Loop to add only new search results
              result.data.forEach(obj => {
                // Only search results with ID
                if (obj.id) {
                  // Final verification
                  if(!tempArrIndex.includes(obj.id)) {
                    tempArrIndex.push(obj.id)
                    finalResult.push(obj)
                  }
                }
              })
            }
          }

          commit('setTools', finalResult)
          commit('setLoading', false)
        } catch (error) {
          console.log(error)
          commit('setLoading', false)
        }
      }
    },
    getters: {
      tools: state => state.tools,
      searchArray: state => state.searchArray,
      tagsOnly: state => state.tagsOnly
    }
  })
}

export default createStore
